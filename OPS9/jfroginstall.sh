#!/bin/bash 

# Jfrog - Ubuntu 20.04
aws ec2 run-instances \
--image-id "ami-01f87c43e618bf8f0" \
--instance-type "t2.medium" \
--count 1 \
--subnet-id "subnet-0feefff108ffdae10" \
--security-group-ids "sg-0aedc686d7c0c95ca" \
--tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=jfrog},{Key=Environment,Value=Development}]' \
--key-name "linux1" \
--user-data file://jfroginstall.txt