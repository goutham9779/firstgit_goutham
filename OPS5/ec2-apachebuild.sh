#!/bin/bash 

# WebServer Of Linux - Ubuntu 20.04
aws ec2 run-instances \
--image-id "ami-009726b835c24a3aa" \
--instance-type "t2.micro" \
--count 1 \
--subnet-id "subnet-0feefff108ffdae10" \
--security-group-ids "sg-0aedc686d7c0c95ca" \
--tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=LinuxServer},{Key=Environment,Value=Development},{Key=ProjectName,Value=SoftoBizDevOpapache2},{Key=ProjectID,Value=20220110},{Key=EmailID,Value=goutham9779@gmail.com},{Key=MobileNo,Value=+919908823070}]' \
--key-name "linux1" \
--user-data file://install-web.txt
