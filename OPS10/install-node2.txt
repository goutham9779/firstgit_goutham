#!/bin/bash

# Setup New Hostname
hostnamectl set-hostname "web-node1"

# Configure New Hostname as part of /etc/hosts file 
echo "`hostname -I | awk '{ print $1}'` `hostname`" >> /etc/hosts

# Update the Repository
sudo apt update